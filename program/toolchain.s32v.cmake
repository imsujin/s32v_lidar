#
# CMake Toolchain file for crosscompiling on ARM.
#
# Target operating system name.
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_INSTALL_PREFIX "/usr/local/aarch64" CACHE STRING " " FORCE)
# Name of C compiler.
set(COMPILER_ROOT "/usr/local/aarch64-linux-gnu/bin/aarch64-linux-gnu-")
set(CMAKE_C_COMPILER ${COMPILER_ROOT}gcc CACHE STRING " " FORCE)
set(CMAKE_CXX_COMPILER ${COMPILER_ROOT}g++ CACHE STRING " " FORCE)

# Where to look for the target environment. (More paths can be added here)
set(CMAKE_FIND_ROOT_PATH /usr/local/aarch64)
#set(CMAKE_SYSROOT /usr/local/aarch64)

# Adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment only.
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# Search headers and libraries in the target environment only.
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

#set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE arm)

set(BOOST_ROOT /usr/local/aarch64/)
set(BOOST_INCLUDEDIR /usr/local/aarch64/include/)
set(BOOST_LIBRARYDIR /usr/local/aarch64/lib)
set(Boost_NO_SYSTEM_PATHS TRUE)
set(Boost_NO_BOOST_CMAKE TRUE)

#set(COMMONAPI_INCLUDE_DIRS /usr/local/aarch64/include)
#set(COMMONAPI_LIBRARIES /usr/local/aarch64/lib)
#set(VSOMEIP_INCLUDE_DIRS /usr/local/aarch64/include)
#set(VSOMEIP_LIBRARIES /usr/local/aarch64/lib)
#link_directories(AFTER /usr/local/aarch64/lib)
include_directories(AFTER /usr/local/aarch64/include)

set(LZ4_INCLUDE_DIRS /usr/local/aarch64/include)
set(LZ4_LIBRARIES /usr/local/aarch64/lib/liblz4.so)
#include_directories(${LZ4_INCLUDE_DIRS})
#target_link_libraries(hello ${LZ4_LIBRARIES})

