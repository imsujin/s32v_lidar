#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include "lidar_worker.h"

using namespace std;


int main(void)
{
    uint16_t loop_count=0;
    vector<LIDAR_INFO> lidar_info;
    LIDAR_INFO _info;
    
    
    // 데이터를 수신 할 lidar 센서 정보 
    _info.sensor_name = "left_lidar";  // 센서 이름으로 원하시는 이름으로 설정하시면 됩니다.
    _info.model = LIDAR_WORKER_VLP16;  // 연결되는 센서 정보로 현제 VLP16만 사용가능합니다.
    _info.port = 5101;                 // lidar 센서 메시지의 destination port를 기입하시면 됩니다.
    
    // 데이터를 수신 범위 설정
    // 면좌표계
    //_info.range.x_low = -0.3; // -2 < x < 6(m) 범위 측정값만 사용
    //_info.range.x_high = 7;
    //_info.range.y_low =  0; // 0 < y < 5(m) 범위 측정값만 사용
    //_info.range.y_high = 5;
    _info.range.z_low = -0.3; // -0.3 < z < 2(m) 범위 측정값만 사용
    _info.range.z_high = 2;
    // 구좌표계
    _info.range.yaw_low = 270; // Lidar 측정 각도 범위 설정 270~90 도 사이의 측정값만 사용  
    _info.range.yaw_high = 90;
    lidar_info.push_back(_info);       // lidar worker를 설정에 사용될 vector에 저장합니다.
                                       // 다른 lidar 센서를 추가하시려면 아래와 같이 다른 센서의 정보를 기입하시면 됩니다.


    _info.sensor_name = "right_lidar";  // 센서 이름으로 원하시는 이름으로 설정하시면 됩니다.
    _info.model = LIDAR_WORKER_VLP16;  // 연결되는 센서 정보로 현제 VLP16만 사용가능합니다.
    _info.port = 5102;                 // lidar 센서 메시지의 destination port를 기입하시면 됩니다.
   
    // 데이터를 수신 범위 설정
    // 면좌표계
    //_info.range.x_low = -0.3; // -2 < x < 6(m) 범위 측정값만 사용
    //_info.range.x_high = 7;
    //_info.range.y_low =  0; // 0 < y < 5(m) 범위 측정값만 사용
    //_info.range.y_high = 5;
    _info.range.z_low = -0.3; // -0.3 < z < 2(m) 범위 측정값만 사용
    _info.range.z_high = 2;
    // 구좌표계
    _info.range.yaw_low = 300; // Lidar 측정 각도 범위 설정 300~60 도 사이의 측정값만 사용  
    _info.range.yaw_high = 60;
     lidar_info.push_back(_info);    
    
    LIDAR_WORKER lworker(lidar_info); // lidar worker 실행

    while(1)
    {
        sleep(1);
        cout<<"========= [loop cnt : "<<loop_count++<<"] ========="<<endl;
    }

    return 0;    
}