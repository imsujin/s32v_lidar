rm -vrf build
mkdir build
cd build
cmake ../ -DCMAKE_TOOLCHAIN_FILE=../toolchain.s32v.cmake
make -j2
