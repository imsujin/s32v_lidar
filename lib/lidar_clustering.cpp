#include "lidar_clustering.h"


uint16_t lidar_clustering(string name,const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud ,pcl::PointCloud<PointXYZIR> &ret_cloud)
{
    static uint16_t count = 0;
    static uint16_t runflag=0;

    pcl::PointCloud<pcl::PointXYZI> laserCloudIn = *cloud;
    pcl::PointCloud<pcl::PointXYZI> laserCloudIn2;
    
    pcl::VoxelGrid<pcl::PointXYZI> vg;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_filtered_v2 (new pcl::PointCloud<pcl::PointXYZI>);
    vg.setInputCloud (laserCloudIn.makeShared());
    vg.setLeafSize (0.5f, 0.5f, 0.5f);		
    vg.filter (*cloud_filtered_v2);			
    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZI>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZI>);
    tree->setInputCloud (cloud_filtered_v2);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZI> ec;
    ec.setClusterTolerance (1.5); 			
    ec.setMinClusterSize (4);				
    ec.setMaxClusterSize (40);				
    ec.setSearchMethod (tree);				
    ec.setInputCloud (cloud_filtered_v2);	
    ec.extract (cluster_indices);


    uint16_t j = 0;
    
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
    {
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
        {
            pcl::PointXYZI pt = cloud_filtered_v2->points[*pit];
            
            PointXYZIR pt2;
            pt2.x = pt.x, pt2.y = pt.y, pt2.z = pt.z;
            pt2.intensity = pt.intensity;
            pt2.r=(float)(j + 1);
            ret_cloud.push_back(pt2);
        }
        j++;
    }
    //ret_cloud.n_obj = j;
    return j;
}