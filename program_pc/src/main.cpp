#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include "lidar_worker.h"

using namespace std;

/*
    본 프로그램은 ./test "udp_port" 로 실행 시키면 됩니다.
    udp port 번호를 기입하지 않을 시 5101 udp port를 사용하게 됩니다.
    4101 udp port로 실행 시 ex) ./test 4101
    333 udp port로 실행 시 ex) ./test 333   
*/


int main(int argc, char **argv)
{
    uint16_t loop_count=0;
    vector<LIDAR_INFO> lidar_info;
    LIDAR_INFO _info;

    _info.sensor_name = "lidar port : ";  
    _info.model = LIDAR_WORKER_VLP16;  
    if(argc>1) 
    {
        _info.sensor_name.append(argv[1]);
        _info.port = stoi(argv[1]);                 
    }
    else 
    {
        _info.sensor_name.append("5101");
        _info.port = 5101;
    }

    // 데이터를 수신 범위 설정
    // 면좌표계
    //_info.range.x_low = -0.3; // -2 < x < 6(m) 범위 측정값만 사용
    //_info.range.x_high = 7;
    //_info.range.y_low =  0; // 0 < y < 5(m) 범위 측정값만 사용
    //_info.range.y_high = 5;
    _info.range.z_low = -0.3; // -0.3 < z < 2(m) 범위 측정값만 사용
    _info.range.z_high = 2;
    // 구좌표계
    _info.range.yaw_low = 270; // Lidar 측정 각도 범위 설정 270~60 도 사이의 측정값만 사용  
    _info.range.yaw_high = 90;
    lidar_info.push_back(_info);       

    LIDAR_WORKER lworker(lidar_info); // lidar worker 실행



    while(1)
    {
        sleep(1);
        cout<<"========= [loop cnt : "<<loop_count++<<"] ========="<<endl;
    }
    return 0;    
}