cmake_minimum_required(VERSION 2.8)
include(FindPkgConfig)
find_package(Boost 1.60.0 COMPONENTS filesystem system REQUIRED)

message(STATUS "Start ..")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DLIDAR_VISUALIZATION_ON -pthread -std=c++0x -Wpointer-arith -Wl,--no-as-needed")



include_directories(
	/usr/include
 	/usr/include/pcl-1.8
	/usr/include/eigen3
	/usr/include/vtk-6.3/
	src
	../lib
)
  
link_directories(
	build
	/usr/local/lib
	/usr/lib
	/usr/lib/x86_64-linux-gnu
)


file(GLOB DH_SRC_FILE src/*.cpp ../lib/*.cpp)

add_executable(test
    ${DH_SRC_FILE}
)
target_link_libraries(test pcl_common pcl_io pcl_search pcl_filters pcl_segmentation pcl_visualization vtkCommonCore-6.3 boost_system)